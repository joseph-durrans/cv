<?php
require_once 'vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./views');

$twig = new \Twig\Environment($loader);

$redirect = $_SERVER['REQUEST_URI'];

switch ($redirect) {
    case '/':
        echo $twig->render('home.twig');
        break;
    case '/projects' :
        echo $twig->render('projects.twig');
        break;
    case '/cv' :
        echo $twig->render('cv.twig');
        break;
    case '/contact' :
        echo $twig->render('contact.twig');
        break;
    case '/contact/thank-you' :
        echo $twig->render('thank-you.twig');
        break;
    default:
        echo $twig->render('404.twig');
        break;
}

exit;