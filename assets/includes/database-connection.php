<?php namespace Database;

class DatabaseConnection{
    private $servername="127.0.0.1";
    private $user="root";
    private $pass="";
    private $dbname="testing";

    function open(){
        $conn = new mysqli($servername, $user, $pass, $dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
    }

    function close(){
        $conn.close();
    }
}
