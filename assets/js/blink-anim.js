function blink(){
    $('.js-underscore').delay(500).fadeTo(0,0).delay(500).fadeTo(0,1, blink);
}

$(document).ready(function() {
    blink();
});