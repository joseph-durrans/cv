function validator(){
    $("form[name='contact_form']").validate({
      rules: {
        name: "required",
        email: {
          required: true,
          email: true
        },
        message: "required"
      },
      messages: {
        name: " Please enter your name",
        email: " Please enter a valid email address",
        message: " Please enter a message"
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
}

$(document).ready(function() {
  validator();
});